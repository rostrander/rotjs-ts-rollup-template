import typescript from 'rollup-plugin-typescript2';
import copy from 'rollup-copy-plugin';

export default {
    input: './src/main.ts',
    output: {
        file: './public/lib/build.js', // output a single application bundle
        name: 'template',
        sourceMap: true,
        sourceMapFile: './public/build.js.map',
        format: 'iife',
        globals: {
            'rot-js': 'ROT'
        }
    },
    plugins: [
        typescript(),
        copy({
            "node_modules/rot-js/dist/rot.js": "public/lib/rot.js",
            "node_modules/rot-js/dist/rot.min.js": "public/lib/rot.min.js",
        })
    ],
    external: ['rot-js'],
}
